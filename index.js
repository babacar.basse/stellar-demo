const express = require("express");
const bodyParser = require("body-parser");
const rp = require("request-promise");
const Stellar = require("stellar-sdk");
const mongoose = require("mongoose");
var _ = require('lodash');
mongoose.Promise = global.Promise;
/* Initialize app and configure bodyParser */

const port = 4000;
const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const listPair = [];
const notifications = [];

/* Global Vars */
const server = new Stellar.Server("https://horizon-testnet.stellar.org");
Stellar.Network.useTestNetwork();

mongoose.connect(
	"mongodb://localhost:27017/stellar-demo",
	{ useNewUrlParser: true }
);

const accountSchema = new mongoose.Schema({
	created_at: Date,
	pin: Number,
	id: String,
	public_key: String,
	secrey_key: String
});

const ModelAccount = mongoose.model("Account", accountSchema);

/* Stellar Interactions */
const createAccount = async (req, res) => {
	const body_req = req.body;
	var _reqexist = await checkAccount(body_req.pin);
	if (_reqexist.exist === true) {
		res.send("account loaded");
		return;
	}
	let pairA = Stellar.Keypair.random();
	const data = {
		created_at: new Date(),
		pin: body_req.pin,
		public_key: pairA.publicKey(),
		secrey_key: pairA.secret()
	};

	// Create Account and request balance on testnet
	await rp.get({
		uri: "https://horizon-testnet.stellar.org/friendbot",
		qs: { addr: pairA.publicKey() },
		json: true
	});

	const account = await server.loadAccount(pairA.publicKey()); // Load newly created account

	// Print balances at account.balances[0].balance
	console.log("\nBalances for account A: " + pairA.publicKey());
	account.balances.forEach(balance => {
		console.log("Type:", balance.asset_type, ", Balance:", balance.balance);
	});
	console.log(JSON.stringify(pairA));

	const myData = new ModelAccount(data);
	myData
		.save()
		.then(item => {
			listPair.push({
				id: data.pin,
				pair: pairA
			});
			res.send("Account created successfully!");
		})
		.catch(err => {
			res.status(400).send("unable to save to database");
		});
};

const checkAccount = (pin) => {
	return new Promise((resolve, reject) => {
		ModelAccount.find({ pin: pin })
			.lean()
			.exec((error, records) => {
				if (error) reject(error);
				else {
					if (records.length > 0) {
						resolve({ exist: true, data: records[0] });
					} else {
						resolve({ exist: false, data: null });
					}
				}
			});
	});
};

const checkAccount_public_key = (public_key) => {
	return new Promise((resolve, reject) => {
		ModelAccount.find({ public_key: public_key })
			.lean()
			.exec((error, records) => {
				if (error) reject(error);
				else {
					if (records.length > 0) {
						resolve({ exist: true, data: records[0] });
					} else {
						resolve({ exist: false, data: null });
					}
				}
			});
	});
};


/* Initiate payment from acc A to acc B */
const makePayment = async (req, res) => {
	const pin_a = req.body.pin_a;
	const pin_b = req.body.pin_b;
	const accountA_exist = await checkAccount(pin_a);
	if (accountA_exist.exist !== true) {
		res.status(400).send("your account not exist");
		return;
	}
	const accountB_exist = await checkAccount(pin_b);
	if (accountB_exist.exist !== true) {
		res.status(400).send("account B not exist");
		return;
	}
	const accountA = await server.loadAccount(accountA_exist.data.public_key);

	console.log(req.body.amount);
	// const accountB = accountB_exist.data;
	const transaction = new Stellar.TransactionBuilder(accountA)
		.addOperation(
			Stellar.Operation.payment({
				destination: accountB_exist.data.public_key,
				asset: Stellar.Asset.native(),
				amount: req.body.amount
			})
		)
		.build();

	var sourceKeys = Stellar.Keypair.fromSecret(accountA_exist.data.secrey_key);
	transaction.sign(sourceKeys);

	// Let's see the XDR (encoded in base64) of the transaction we just built
	/*
	console.log(
		"\nXDR format of transaction: ",
		transaction.toEnvelope().toXDR("base64")
	);
	*/

	try {
		const transactionResult = await server.submitTransaction(transaction);
		/*
		console.log("\n\nSuccess! View the transaction at: ");
		console.log(transactionResult._links.transaction.href);
		console.log(JSON.stringify(transactionResult, null, 2));
		*/
		const msg = {
			from: transaction.source,
			to: transaction.operations[0].destination,
			amount: transaction.operations[0].amount,
			type: transaction.operations[0].type,
			asset_type: transaction.operations[0].asset.code,
			id: new Date().getTime()
		};
		notify(msg);
		res.send("Transaction successful!");
	} catch (err) {
		console.log("An error has occured:");
		console.log(err);
		res.send("Transaction failed");
	}
};

const accountInfo = async(req, res) => {
	const accountA_exist = await checkAccount(req.body.pin);
	if (accountA_exist.exist !== true) {
		res.status(400).send("your account not exist");
		return;
	}
	const accountA = await server.loadAccount(accountA_exist.data.public_key);
	res.send(accountA);
};

/* Retrieve operation history for account */
const getOperation = async (req, res) => {
	const accountA_exist = await checkAccount(req.body.pin);
	if (accountA_exist.exist !== true) {
		res.status(400).send("your account not exist");
		return;
	}
	const accountA = await server.loadAccount(accountA_exist.data.public_key);
	let _payments = [];
	const payments = await server.operations()
		.forAccount(accountA.accountId())
		.order("desc")
		.call();
	_payments = _.map(payments.records, payment => {
		return _transformPaymentFields(payment);
	});
	res.send(_payments);
};

function _transformPaymentFields(payment) {
	if (payment.type === 'create_account') {
		payment.from   = payment.funder;
		payment.to     = payment.account;
		payment.amount = payment.starting_balance;
	}

	payment.direction = (payment.from === this.address) ? 'out' : 'in';
	payment.display_address = (payment.from === this.address) ? payment.to : payment.from;
	let sign = payment.direction === 'in' ? '+' : '-';
	payment.display_amount = `${sign}${payment.amount}`;

	if (payment.asset_code) {
		payment.display_asset_code = payment.asset_code;
	} else {
		payment.display_asset_code = 'XLM';
	}

	payment.link = payment._links.self.href;

	return payment;
}


function notify(message) {
	notif = notifications.filter((id) => {
		return id === message.id;
	});
	if (notif.length) {
		console.log('FOR US BUT ALREAD SENT');
		return;
	}
	notifications.push(message.id);
	console.log('=========================FOR US====================');
	console.log('new paiement');
	io.emit('new paiement', {
		from: message.from,
		to: message.to,
		amount: message.amount,
		type: message.type,
		asset: message.asset_type
	});
}

io.on('connection', function(socket){
	console.log('a user connected');
	socket.on('disconnect', function(){
		console.log('user disconnected');
	});
});

/* CORS */
app.use((req, res, next) => {
	// Website you wish to allow to connect
	res.setHeader("Access-Control-Allow-Origin", "*");

	// Request methods you wish to allow
	res.setHeader(
		"Access-Control-Allow-Methods",
		"GET, POST, OPTIONS, PUT, PATCH, DELETE"
	);

	// Request headers you wish to allow
	res.setHeader(
		"Access-Control-Allow-Headers",
		"Origin,X-Requested-With,content-type"
	);

	// Pass to next layer of middleware
	next();
});

/* API Routes */
app.post("/", createAccount);
app.post("/payment", makePayment);
app.post("/getHistory", getOperation);
app.post("/account", accountInfo);

/* Serve API */
var instance = http.listen(port, () => {
	server.operations()
		.stream({
			onmessage: (data) => {
				// console.log(data.from);
			}
		})
	;
	console.log(`Stellar test app listening on port ${port}!`);
});
